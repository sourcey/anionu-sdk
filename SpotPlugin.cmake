#
### Macro: define_spot_executable
#
# This template defines a Spot plug-in.
#
macro(define_spot_plugin name)
    project(${name})

    file(GLOB lib_srcs "*.cpp")
    file(GLOB lib_hdrs "*.h*")

    source_group("Src" FILES ${lib_srcs})
    source_group("Include" FILES ${lib_hdrs})

    include_directories(${LibSourcey_INCLUDE_DIRS})
    link_directories(${LibSourcey_LIBRARY_DIRS})

    set_default_project_dependencies(${ARGN}) #uv base anionu spotapi 
    #include_sourcey_modules(uv base anionu spotapi ${ARGN}) # Media Net JSON Media  HTTP
    include_directories(
      "${LibSourcey_INSTALL_DIR}/lib"
      "${CMAKE_SOURCE_DIR}/src/anionu-sdk/spot-api/include")

    add_library(${name} MODULE ${lib_srcs} ${lib_hdrs})

    #status("  Linking Spot plugin ${name}")
    #status("    Libraries:               ${LibSourcey_INCLUDE_LIBRARIES}")
    #status("    Library Dirs:            ${LibSourcey_LIBRARY_DIRS}")
    #status("    Include Dirs:            ${LibSourcey_INCLUDE_DIRS}")
    #status("    Dependencies:            ${LibSourcey_BUILD_DEPENDENCIES}")

    target_link_libraries(${name} ${LibSourcey_INCLUDE_LIBRARIES})

    if(ENABLE_SOLUTION_FOLDERS)
      set_target_properties(${name} PROPERTIES FOLDER "plugins")
    endif()
    set_target_properties(${name} PROPERTIES DEBUG_POSTFIX "${LibSourcey_DEBUG_POSTFIX}")

    if (NOT ${name}_EXECUTABLE_NAME)
      set(${name}_EXECUTABLE_NAME ${name})
    endif()
    if (NOT ${name}_DEBUG_POSTFIX AND NOT ${name}_DEBUG_POSTFIX STREQUAL "")
      set(${name}_DEBUG_POSTFIX ${LibSourcey_DEBUG_POSTFIX})
    endif()
    set_target_properties(${name} PROPERTIES
      OUTPUT_NAME ${${name}_EXECUTABLE_NAME}
      DEBUG_POSTFIX "${${name}_DEBUG_POSTFIX}")

    install(TARGETS ${name}
      #DESTINATION "${CMAKE_INSTALL_PREFIX}/spot/plugins/${name}"
      DESTINATION "${CMAKE_INSTALL_PREFIX}/plugins"
      COMPONENT main)

endmacro()
